package com.etaskify.service.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.BDDMockito.given;

import com.etaskify.dto.request.OrganizationRequestDTO;
import com.etaskify.dto.request.UserDTO;
import com.etaskify.dto.response.ApiResponse;
import com.etaskify.dto.response.UserResponseDTO;
import com.etaskify.enums.RoleName;
import com.etaskify.exception.custom.EntityNotFoundException;
import com.etaskify.mapper.OrganizationMapper;
import com.etaskify.model.Organization;
import com.etaskify.model.Role;
import com.etaskify.model.Task;
import com.etaskify.model.User;
import com.etaskify.repository.OrganizationRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


import static org.assertj.core.api.Assertions.assertThatThrownBy;


class OrganizationServiceImplTest {
     @Mock
    private  OrganizationRepository organizationRepository;

     @Mock
   private OrganizationMapper organizationMapper;

     private OrganizationServiceImpl organizationServiceUnderTest;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        organizationServiceUnderTest = new OrganizationServiceImpl(organizationRepository);
    }
    @Test
    void createTest(){
        OrganizationRequestDTO mockOrganizationRequestDto = OrganizationRequestDTO.builder()
                .address("Baku")
                .phoneNumber("0123701520")
                .user(UserDTO.builder()
                        .password("1345456")
                        .build())
                .orgName("TestOrg")
                .build();

        Organization mockOrganization = Organization.builder()
                .name("tech")
                .build();
        mockOrganization.setId(2L);

        User user = User.builder()
                .name("azer")
                .password("12345")
                .organization(mockOrganization)
                .build();

        given(organizationMapper.mapDtoToEntity(mockOrganizationRequestDto)).willReturn(mockOrganization);
        given(organizationRepository.save(mockOrganization)).willReturn(notNull());

        //when
        ApiResponse<?> createTaskResponse = organizationServiceUnderTest.create(mockOrganizationRequestDto);

        assertTrue(createTaskResponse.isSuccess());
    }
    @Test
    void getUsersOfOrganization() {

        Task taskMock = Task.builder()
                .deadLine("2 hours")
                .endDate(LocalDateTime.now())
                .build();

        List<Task> mockTaskList = List.of(taskMock);
        Organization organizationMock = Organization.builder()
                .name("test org")
                .tasks(mockTaskList)
                .build();

        User userMock = User.builder()
                .name("Azer")
                .role(Role.builder()
                        .name(RoleName.USER)
                        .build())
                .organization(organizationMock)
                .build();

        Organization mockOrganization = Organization.builder()
                .name("test")
                .users(List.of(userMock))
                .build();
        mockOrganization.setId(2L);


        UserResponseDTO mockUserResponseDto= UserResponseDTO.builder()
                .name("orxan")
                .roleName(RoleName.USER)
                .build();

        given(organizationRepository.findById(2L)).willReturn(Optional.of(mockOrganization));

        ApiResponse<?> tasksForUserResponse = organizationServiceUnderTest.getUsersOfOrganization(2L);

        //then
        assertTrue(tasksForUserResponse.isSuccess());

    }
    @Test
    void getTasksForUserTestUserNotExist() {

        //given

        String email = "azer@gmail.com";

        Task taskMock = Task.builder()
                .deadLine("2 hours")
                .endDate(LocalDateTime.now())
                .build();

        Organization organizationMock = Organization.builder()
                .name("test org")
                .tasks(List.of(taskMock))
                .build();

        User userMock = User.builder()
                .name("Azer")
                .organization(organizationMock)
                .email(email)
                .build();

        given(organizationRepository.findById(2L)).willReturn(Optional.of(organizationMock));

        //when

        //then
        assertThatThrownBy(
                () -> organizationServiceUnderTest.getUsersOfOrganization(3L))
                .isInstanceOf(EntityNotFoundException.class)
                .hasMessageContaining("Organization was not found for parameters");

    }
}